import { Address, toNano } from 'ton-core';
import { OrderMaster } from '../wrappers/OrderMaster';
import { NetworkProvider, sleep } from '@ton-community/blueprint';
import { waitTx, getLogicalTime } from '../wrappers/utils';
import { getContractsForChainOrThrow, ChainType } from "../wrappers/contracts";

export async function run(provider: NetworkProvider, args: string[]) {
    const ui = provider.ui();

    const orderIndex = Number(args.length > 0 ? args[0] : await ui.input('Order index'));
    console.log("🚀 ~ file: sendAcceptOrder.ts:11 ~ run ~ orderIndex:", orderIndex);


    const contracts = getContractsForChainOrThrow(ChainType.testnet, provider.api());
    const orderItemProxied = await contracts.getOrderItemProxied(orderIndex, provider.sender().address!);

    const lt = await getLogicalTime(provider.api(), orderItemProxied.address);


    await orderItemProxied.sendAcceptOrder(provider.sender(), {
        value: toNano('0.1'),
    });

    ui.write('Waiting for result');

    const addr = await contracts.getOrderItemAddress(orderIndex);
    console.log("🚀 ~ file: sendAcceptOrder.ts:27 ~ run ~ addr:", addr);
    const index = await waitTx(provider.api(), addr, orderItemProxied.address, lt);

    ui.write(`Result: ${!!index}`);


    ui.clearActionPrompt();
    ui.write(`Finished!`);
}
