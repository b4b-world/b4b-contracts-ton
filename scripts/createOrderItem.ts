import { Address, toNano, fromNano } from 'ton-core';
import { OrderMaster } from '../wrappers/OrderMaster';
import { NetworkProvider, sleep } from '@ton-community/blueprint';
import { getLogicalTime, waitCreateOrder } from '../wrappers/utils';
import { getContractsForChainOrThrow, ChainType } from "../wrappers/contracts";

export async function run(provider: NetworkProvider, args: string[]) {
    const ui = provider.ui();

    // const address = Address.parse(args.length > 0 ? args[0] : await ui.input('B4B address'));

    // if (!(await provider.isContractDeployed(address))) {
    //     ui.write(`Error: Contract at address ${address} is not deployed!`);
    //     return;
    // }
    const contracts = getContractsForChainOrThrow(ChainType.testnet, provider.api());
    const orderMasterContract = contracts.getOrderMaster();

    const lt = await getLogicalTime(provider.api(), orderMasterContract.address);


    await orderMasterContract.sendCreateOrder(provider.sender(), provider.sender().address!, {
        value: toNano('0.3'),
    });

    ui.write('Waiting for result');

    const index = await waitCreateOrder(provider.api(), orderMasterContract.address, provider.sender().address!, lt);

    ui.write(`Result: ${index}`);

    const orderItemContract = await contracts.getOrderItem(index);
    console.log(index, orderItemContract.address);

    const balance = await provider.api().getBalance(orderMasterContract.address);
    console.log("🚀 ~ file: createOrderItem.ts:36 ~ run ~ balance:", fromNano(balance));
    
    const balanceProxy = await provider.api().getBalance(
        await orderMasterContract.getProxyAddress(provider.sender().address!)
    );
    console.log("🚀 ~ file: createOrderItem.ts:41 ~ run ~ balanceProxy:", fromNano(balanceProxy));

    ui.clearActionPrompt();
    ui.write(`Finished!`);
}
