import { toNano } from 'ton-core';
import { OrderMaster } from '../wrappers/OrderMaster';
import { compile, NetworkProvider } from '@ton-community/blueprint';

export async function run(provider: NetworkProvider) {
    const orderMaster = provider.open(
        OrderMaster.createFromConfig(
            {
                itemCode: await compile('OrderItem'),
                proxyCode: await compile('Influencer'),
                nextItemIndex: 1,
                owner: provider.sender().address!,
            },
            await compile('OrderMaster')
        )
    );

    await orderMaster.sendDeploy(provider.sender(), toNano('0.05'));

    await provider.waitForDeploy(orderMaster.address);

    // console.log('ID', await b4B.getID());
}
