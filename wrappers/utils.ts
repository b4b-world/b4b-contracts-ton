import { Address, TonClient, TransactionDescriptionGeneric, Cell } from "ton";

function sleep(ms: number) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
}

export async function getLogicalTime(client: TonClient, addr: Address): Promise<string> {
    const state = await client.getContractState(addr);
    const lt = state.lastTransaction?.lt ?? '0';

    return lt;
}

export async function waitTx(client: TonClient, dstAddr: Address, srcAddr: Address, lt: string): Promise<number> {
    const txs = await client.getTransactions(dstAddr, { 
        limit: 10, 
        lt,
        inclusive: true
    });

    const filteredTxs = txs
        .filter(tx => tx.lt > BigInt(lt))
        .filter(tx => {
            return tx.inMessage?.info.src?.toString().toLowerCase() === srcAddr.toString().toLowerCase();
        });

    if (filteredTxs.length === 0) {
        await sleep(10000);
        return waitTx(client, dstAddr, srcAddr, lt);
    }

    return (filteredTxs[0].description as TransactionDescriptionGeneric).aborted == false ? 1 : 0;
}

export async function waitCreateOrder(client: TonClient, dstAddr: Address, srcAddr: Address, lt: string): Promise<number> {
    const txs = await client.getTransactions(dstAddr, { 
        limit: 10, 
        lt,
        inclusive: true
    });

    const filteredTxs = txs
        .filter(tx => tx.lt > BigInt(lt))
        .filter(tx => {
            return tx.inMessage?.info.src?.toString().toLowerCase() === srcAddr.toString().toLowerCase();
        });

    if (filteredTxs.length == 0) {
        await sleep(5000);
        return waitCreateOrder(client, dstAddr, srcAddr, lt);
    }

    for (const msg of filteredTxs[0].outMessages.values()) {
        if (msg.body.depth() == 0) continue;
        const outMessageInitData = msg.init?.data;
        if (outMessageInitData) {
            const index = outMessageInitData?.beginParse().loadUint(64);
            return index;
        }
    }

    return 0;
}