import { CompilerConfig } from '@ton-community/blueprint';

export const compile: CompilerConfig = {
    targets: ['contracts/order_item.fc'],
};
