import { Address, beginCell, Cell, Contract, ContractProvider, Sender, toNano, Builder } from "ton-core";

enum OrderItemOp {
    AcceptOrder = 0x35ca7eab,
    CompleteOrder = 0xcf90d618,
    AproveOrder = 0x585a22d4
}

export class OrderItem implements Contract {
    constructor(readonly address: Address, readonly proxiedAddr?: Address, readonly init?: { code: Cell; data: Cell }) {}

    static createFromAddress(address: Address, proxiedAddr?: Address) {
        return new OrderItem(address, proxiedAddr);
    }

    async sendAcceptOrder(provider: ContractProvider, via: Sender, params?: Partial<{
        value: bigint,
    }>) {
        await provider.internal(via, {
            value: params?.value ?? toNano('0.05'),
            body: (this.proxiedAddr ? beginCell().storeAddress(this.proxiedAddr) : beginCell())
                .storeUint(OrderItemOp.AcceptOrder, 32) // op
                .storeUint(0, 64) // query id
                .endCell()
        });
    }

    async sendCompleteOrder(provider: ContractProvider, via: Sender, params?: Partial<{
        value: bigint
    }>) {
        await provider.internal(via, {
            value: params?.value ?? toNano('0.05'),
            body: (this.proxiedAddr ? beginCell().storeAddress(this.proxiedAddr) : beginCell())
                .storeUint(OrderItemOp.CompleteOrder, 32) // op
                .storeUint(0, 64) // query id
                .endCell()
        });
    }

    async sendAproveOrder(provider: ContractProvider, via: Sender, params?: Partial<{
        value: bigint
    }>) {
        await provider.internal(via, {
            value: params?.value ?? toNano('0.05'),
            body: beginCell()
                .storeUint(OrderItemOp.AproveOrder, 32) // op
                .storeUint(0, 64) // query id
                .endCell()
        });
    }

    async getBalance(provider: ContractProvider): Promise<number> {
        const { stack } = await provider.get('get_self_balance', [])
        return stack.readNumber();
    }

    async getData(provider: ContractProvider): Promise<any> {
        const { stack } = await provider.get('get_order_data', []);
        const data = stack.readNumber();
        // const data1 = stack.readAddress();
        // data.beginParse().loadAddress().end
        console.log(data);
    }
}