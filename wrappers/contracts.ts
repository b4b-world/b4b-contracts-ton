import { TonClient } from 'ton';
import { Address } from 'ton-core';
import { OrderMaster } from "./OrderMaster"
import addresses from './addresses.json';
import { OrderItem } from './OrderItem';

export enum ChainType {
    testnet = 'testnet',
    mainnet = 'mainnet'
}

export interface ContractAddresses {
    OrderMaster: string;
}

function getContractAddressesForChainOrThrow(chain: ChainType) {
    const _addresses: Record<string, ContractAddresses> = addresses;
    if (!_addresses[chain]) {
        throw new Error(`Unknown chain (${chain}). No known contracts have been deployed on this chain.`);
    }
    return _addresses[chain];
}

export function getContractsForChainOrThrow(chain: ChainType, client: TonClient) {
    const addresses = getContractAddressesForChainOrThrow(chain);

    const orderMaster = client.open(
        OrderMaster.createFromAddress(Address.parse(addresses.OrderMaster))
    );

    async function getOrderItem(index: number) {
        const address = await orderMaster.getOrderAddress(index);
        return client.open(
            OrderItem.createFromAddress(address)
        );
    }

    async function getOrderItemAddress(index: number) {
        const address = await orderMaster.getOrderAddress(index);
        return address;
    }

    async function getOrderItemProxied(orderIndex: number, influencerAddress: Address) {
        const proxyAddress = await orderMaster.getProxyAddress(influencerAddress);
        const address = await orderMaster.getOrderAddress(orderIndex);
        return client.open(
            OrderItem.createFromAddress(proxyAddress, address)
        );
    }

    return {
        getOrderMaster: () => { return orderMaster; },
        getOrderItem,
        getOrderItemProxied,
        getOrderItemAddress
    }
}