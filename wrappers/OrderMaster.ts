import { Address, beginCell, Cell, Contract, contractAddress, ContractProvider, Sender, toNano, SendMode, Slice } from "ton-core";
import { OrderItem } from "./OrderItem";

export type OrderMasterConfig = {
    nextItemIndex: number,
    owner: Address,
    itemCode: Cell,
    proxyCode: Cell
}

enum OrderMasterOp {
    CreateOrder = 0xa39843f4,
    CreateProxy = 0x25c319d2
}

export class OrderMaster implements Contract {
    constructor(readonly address: Address, readonly init?: { code: Cell; data: Cell }) {}

    static createFromAddress(address: Address) {
        return new OrderMaster(address);
    }

    static createFromConfig(config: OrderMasterConfig, code: Cell, workchain = 0) {
        const nextItemIndex = config.nextItemIndex ?? 0;
        const data = beginCell()
            .storeAddress(config.owner)
            .storeUint(nextItemIndex, 64)
            .storeRef(config.itemCode)
            .storeRef(config.proxyCode)
            .endCell();
        const init = { code, data }
        const address = contractAddress(workchain, init)
        return new OrderMaster(address, init);
    }

    async sendDeploy(provider: ContractProvider, via: Sender, value: bigint) {
        await provider.internal(via, {
            value,
            sendMode: SendMode.PAY_GAS_SEPARATELY,
            // body: beginCell().endCell(),
        });
    }

    async sendCreateOrder(provider: ContractProvider, via: Sender, influencer: Address, params?: Partial<{
        value: bigint // price + all fees
        itemValue: bigint // price + order_item fees (strg + processing)
    }>) {
        const body = beginCell()
            .storeUint(OrderMasterOp.CreateOrder, 32) // op
            .storeUint(0, 64) // query id
            .storeAddress(influencer)
            .endCell();

            await provider.internal(via, {
                value: params?.value ?? toNano('0.05'),
                body: body
            });
    }

    async sendCreateProxy(provider: ContractProvider, via: Sender, influencer: Address, params?: Partial<{
        value: bigint // price + all fees
        itemValue: bigint // price + order_item fees (strg + processing)
    }>) {
        const body = beginCell()
            .storeUint(OrderMasterOp.CreateProxy, 32) // op
            .storeUint(0, 64) // query id
            .storeAddress(influencer)
            .endCell();

            await provider.internal(via, {
                value: params?.value ?? toNano('0.05'),
                body: body
            });
    }

    async getBalance(provider: ContractProvider): Promise<number> {
        const { stack } = await provider.get('get_self_balance', [])
        return stack.readNumber();
    }

    async getOrderAddress(provider: ContractProvider, index: number): Promise<Address> {
        const res = await provider.get('get_order_address', [{ type: 'int', value: BigInt(index) }]);
        return res.stack.readAddress();
    }

    async getProxyAddress(provider: ContractProvider, address: Address): Promise<Address> {
        const addrCell = beginCell().storeAddress(address).endCell();
        const res = await provider.get('get_proxy_address', [{ type: 'slice', cell: addrCell }]);
        return res.stack.readAddress();
    }
}