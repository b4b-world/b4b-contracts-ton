# B4B.WORLD contracts - TON

## Contracts
1. influencer.fc - represents the influencer and stores the influencer's rating.
    The idea behind the influencer proxy contract is to ensure that only influencers with a verified audience have access to our platform. That is why the process of influencer’s proxy creation is under B4B control.
2. order_item.fc - represents ad order, stores payments #TONs, mints influencer's rating.
3. order_master.fc - creates orders and influencers.

## How to?
1. [Brand] Create ad order 
```typescript
const STRG_FEE = 0.05;
const FWD_FEE = 0.02;
const COM_FEE = 0.05;

const client = new TonClient(...);
const contracts = getContractsForChainOrThrow(ChainType.testnet, client);

const orderMasterContract = contracts.getOrderMaster();

const lt = await getLogicalTime(client, orderMasterContract.address);
await orderMasterContract.sendCreateOrder(provider.sender(), provider.sender().address!, {
    value: toNano(PRICE_IN_TONCOINS + 2 * STRG_FEE + 2 * FWD_FEE + COM_FEE),
});

const orderId = await waitCreateOrder(provider.api(), orderMasterContract.address, provider.sender().address!, lt);
```
2. [Influencer] Accept ad order
```typescript
const orderItemContract = contracts.getOrderItemProxied(orderId, influencerAddress);
const lt = await getLogicalTime(provider.api(), orderItemContract.address);

await orderItem.sendAcceptOrder(provider.getSender(), {
    value: FWD_FEE
});

const orderAddr = await contracts.getOrderItemAddress(orderId);
const result = await waitTx(provider.api(), orderAddr, orderItemContract.address, lt);
// 1 - true, 0 - false

```

3. [Influencer] Complete ad order
```typescript
const orderItemContract = contracts.getOrderItemProxied(orderId, influencerAddress);
const lt = await getLogicalTime(provider.api(), orderItemContract.address);

await orderItem.sendCompleteOrder(provider.getSender(), {
    value: FWD_FEE
});

const orderAddr = await contracts.getOrderItemAddress(orderId);
const result = await waitTx(provider.api(), orderAddr, orderItemContract.address, lt);
// 1 - true, 0 - false

```

3. [Brand] Aprove ad order
```typescript
const orderItemContract = contracts.getOrderItem(orderId);
const lt = await getLogicalTime(provider.api(), orderItemContract.address);

await orderItem.sendAproveOrder(provider.getSender(), {
    value: FWD_FEE
});

const result = await waitTx(provider.api(), orderItemContract.address, brandAddr, lt);
// 1 - true, 0 - false

```

# License
MIT
