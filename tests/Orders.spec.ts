import { beginCell, SendMode, toNano, Cell, fromNano, Address } from "ton-core"
import { Blockchain, SandboxContract, TreasuryContract } from "@ton-community/sandbox"
import { OrderMaster } from "../wrappers/OrderMaster"
import { OrderItem } from "../wrappers/OrderItem"
import "@ton-community/test-utils" // register matchers
import { compile, sleep } from '@ton-community/blueprint';

describe('Orders', () => {
    let code: Cell;
    let itemCode: Cell;
    let proxyCode: Cell;

    beforeAll(async () => {
        code = await compile('OrderMaster');
        itemCode = await compile('OrderItem');
        proxyCode = await compile('Influencer');
    });

    let blockchain: Blockchain;
    let orderMaster: SandboxContract<OrderMaster>;
    let deployer: SandboxContract<TreasuryContract>;

    beforeEach(async () => {
        blockchain = await Blockchain.create();
        deployer = await blockchain.treasury('admin');

        orderMaster = blockchain.openContract(
            OrderMaster.createFromConfig(
                {
                    nextItemIndex: 1,
                    owner: deployer.address,
                    itemCode,
                    proxyCode
                },
                code,
                0
            )
        );

        const deployResult = await orderMaster.sendDeploy(deployer.getSender(), toNano('1.0'));

        expect(deployResult.transactions).toHaveTransaction({
            from: deployer.address,
            to: orderMaster.address,
            deploy: true,
            success: true
        });
    });


    it('should work with proxy', async () => {
        const brand = await blockchain.treasury('brand', { balance: toNano('100') });
        const influencer = await blockchain.treasury('influencer', { balance: toNano('10') });

        // const createProxyResult = await orderMaster.sendCreateProxy(brand.getSender(), influencer.address, {
        //     value: toNano('0.1'),
        //     // itemValue: toNano('5.05')
        // });

        const proxyAddress = await orderMaster.getProxyAddress(influencer.address);
        console.log(proxyAddress);

        // expect(createProxyResult!.transactions).toHaveTransaction({
        //     from: orderMaster.address,
        //     to: proxyAddress,
        //     success: true,
        //     deploy: true
        // });

        const createOrderResult = await orderMaster.sendCreateOrder(brand.getSender(), influencer.address, {
            value: toNano('5.2'),
        });

        const orderItemAddress = await orderMaster.getOrderAddress(1);
        const orderItem = blockchain.openContract(OrderItem.createFromAddress(proxyAddress, orderItemAddress));
        const orderItemBrand = blockchain.openContract(OrderItem.createFromAddress(orderItemAddress));

        expect(createOrderResult!.transactions).toHaveTransaction({
            from: orderMaster.address,
            to: orderItemAddress,
            success: true,
            deploy: true
        });

        const balance = (await blockchain.getContract(proxyAddress)).balance;
        console.log("🚀 ~ file: Orders.spec.ts:127 ~ it.only ~ balance:", fromNano(balance));
        const balanceOrder = (await blockchain.getContract(orderItemAddress)).balance;
        console.log("🚀 ~ file: Orders.spec.ts:127 ~ it.only ~ balanceOrder:", fromNano(balanceOrder));

        const resultAccept = await orderItem.sendAcceptOrder(influencer.getSender(), {
            value: toNano('0.02')
        });

        // await sleep(6000);
        expect(resultAccept!.transactions).toHaveTransaction({
            from: proxyAddress,
            to: orderItemAddress,
            success: true,
            // deploy: true
        });

        const resultComplete = await orderItem.sendCompleteOrder(influencer.getSender(), {
            value: toNano('0.02'),
        });
        expect(resultComplete.transactions).toHaveTransaction({
            from: proxyAddress,
            to: orderItemAddress,
            success: true,
        });

        const resultAprove = await orderItemBrand.sendAproveOrder(brand.getSender(), {
            value: toNano('0.02')
        });
        // expect(resultAprove.transactions).toHaveTransaction({
        //     from: brand.address,
        //     to: orderItem.address,
        //     success: true,
        // });

        const balanceInfluence = (await blockchain.getContract(influencer.address)).balance;
        console.log("🚀 ~ file: Orders.spec.ts:166 ~ it.only ~ balanceInfluence:", fromNano(balanceInfluence));
    });
})